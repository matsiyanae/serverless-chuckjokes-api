const { GraphQLError } = require('graphql')
const { v4 } = require('uuid')
const { ApolloServer: ApolloServerLambda } = require('apollo-server-lambda')
const { ApolloServer } = require('apollo-server')

const ChuckAPI = require('./src/chuckAPI')
const resolvers = require('./src/resolvers')
const typeDefs = require('./src/typDefs')
// const { RedisCache } = require('apollo-server-cache-redis')

require('dotenv').config()
const config = {
  typeDefs,
  resolvers,
  dataSources: () => ({
    ChuckAPI: new ChuckAPI()
  }),
  playground: true,
  introspection: true,
  formatError: () => {
    // Mask all errors
    const errId = v4()
    return new GraphQLError(`Internal Error: ${errId}`)
  }
  // cache: new RedisCache({
  //   host: 'redis-server'
  // })
}
const lambda = new ApolloServerLambda(config)
const server = new ApolloServer(config)

exports.server = server
exports.graphqlHandler = lambda.createHandler()
