const { server } = require('./handler')
const port = process.env.PORT || 8081

server.listen({ port }).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`)
})
