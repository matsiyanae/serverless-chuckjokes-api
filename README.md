# Chuck Jokes API

Serve jokes from the chuck API

https://api.chucknorris.io/jokes/categories

https://api.chucknorris.io/jokes/random?category={category}

## Getting Started

These instructions will get the running api on localhost.

You can configure your AWS credentials and deploy the app to AWS Lambda through bitbucket pipelines

### Prerequisites

[Node JS](https://nodejs.org)

### Installing

Run the following commands

```
npm install
```

And run

```
npm start
```

Go to http://localhost:8081/


Query data

Get all categories

```
query {
  categories
}
```

Get joke by category

```
query {
 joke(category:"science") {
   id
   created_at
   updated_at
   url
   value
   icon_url
   categories
 }
}
```

## Running tests

```
npm test
```

### Code compliance

Install [Standard](https://standardjs.com/) and run this command

```
standard --fix
```

## Deployment

This app is configured for development deployment.

## TODO

Configure Auth for users to access protected resourses.

Configure Logging to trace errors in the system.

Configure Logging to trace service usage.

## Built With

- [Apollo Server](https://www.apollographql.com/docs/apollo-server/)
- [GraphQL](https://graphql.org/)
- [Node JS](https://nodejs.org)
- [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines)
- [Redis - Apollo](https://www.npmjs.com/package/apollo-server-cache-redis)
