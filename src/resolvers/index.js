module.exports = {
  Query: {
    joke: (root, { category }, { dataSources }) => dataSources.ChuckAPI.getRandomJoke(category),
    categories: (root, args, { dataSources }) => dataSources.ChuckAPI.getCategories()
  }
}
