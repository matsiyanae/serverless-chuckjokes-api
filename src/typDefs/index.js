const { gql } = require('apollo-server-lambda')

module.exports = gql`
  type Joke {
    id: String!
    created_at: String!
    updated_at: String!
    url: String!
    value: String!
    icon_url: String!
    categories: [String]!
  }
  type Query {
    joke(category: String!): Joke
    categories: [String]
  }
`
