const { RESTDataSource } = require('apollo-datasource-rest')
class ChuckAPI extends RESTDataSource {
  constructor () {
    super()
    this.baseURL = 'https://api.chucknorris.io/jokes/'
  }

  async getCategories () {
    const categories = this.get('categories')
    return categories
  }

  async getRandomJoke (category) {
    const result = await this.get('random/', {
      category
    })
    return result
  }
}
module.exports = ChuckAPI
