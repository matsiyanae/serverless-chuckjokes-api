const rp = require('request-promise')
const { server } = require('../handler')
const port = process.env.PORT || 8081

describe('Checking categories', () => {

  beforeAll(() => {
    server.listen({ port }).then(({ url }) => {
      console.log(`🚀 Server test ready at ${url}`)
    })
  })

  test('fetch categories', async (done) => {
    const query = `
          query {
              categories
          }
      `
    const categories = await rp({ method: 'POST', uri: 'http://localhost:' + port, body: { query }, json: true })
    expect(Array.isArray(categories.data.categories)).toBe(true)
    done()
  })

  test('fetch category joke', async (done) => {
    const query = `
          query {
            joke(category:"science") {
              id
              created_at
              updated_at
              url
              value
              icon_url
              categories
            }
          }
        `
    const joke = await rp({ method: 'POST', uri: 'http://localhost:' + port, body: { query }, json: true })
    expect(typeof joke.data.joke.id).toBe('string')
    done()
  })

  afterAll(done => {
    server.close(done)
  })

})
